package be.kdg.pro3.profilecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfileCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfileCodeApplication.class, args);
	}

}
