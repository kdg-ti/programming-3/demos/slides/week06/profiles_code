package be.kdg.pro3.profilecode.repository;

import be.kdg.pro3.profilecode.domain.Person;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonJdbcTemplateRepository implements PersonRepository {

	private JdbcTemplate jdbcTemplate;

	public PersonJdbcTemplateRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<Person> findByName(String name) {
		return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
			(rs, rowNum) -> new Person(rs.getInt("id"),
				rs.getString("name"),
				rs.getString("firstname"),
				rs.getString("remark")),
			name);
	}
}
