package be.kdg.pro3.profilecode.repository;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class PostgresLoadData {

	private static final Logger log = LoggerFactory.getLogger(PostgresLoadData.class);

	private JdbcTemplate jdbcTemplate;

	public PostgresLoadData(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@PostConstruct
	public void loadData() {
		jdbcTemplate.update("DROP TABLE IF EXISTS PERSONS");
		jdbcTemplate.update("""
			CREATE TABLE PERSONS(
				ID  SERIAL PRIMARY KEY,
				NAME CHARACTER VARYING(100) NOT NULL,
				FIRSTNAME CHARACTER VARYING(50)  NOT NULL,
				REMARK CHARACTER VARYING(256)
			);
			""");
		jdbcTemplate.update("""
   
			INSERT INTO PERSONS(NAME, FIRSTNAME,REMARK)
   VALUES ('JONES', 'JACK','of all trades'),
          ('POTTER', 'JACK','Lilly''s dad'),
          ('POTTER', 'MIA','Lilly''s mum'),
          ('REED', 'JACK','union');
          """);
	}
	}

