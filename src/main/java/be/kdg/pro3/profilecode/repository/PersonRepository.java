package be.kdg.pro3.profilecode.repository;



import be.kdg.pro3.profilecode.domain.Person;

import java.util.List;

public interface PersonRepository {
	List<Person> findByName(String name);

}
