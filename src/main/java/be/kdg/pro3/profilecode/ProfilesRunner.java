package be.kdg.pro3.profilecode;

import be.kdg.pro3.profilecode.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ProfilesRunner implements CommandLineRunner {
	private PersonRepository personRepository;

	public ProfilesRunner(	 PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("\n>> ProfilesRunner");
		System.out.println(personRepository.findByName("POTTER"));
	}
}
