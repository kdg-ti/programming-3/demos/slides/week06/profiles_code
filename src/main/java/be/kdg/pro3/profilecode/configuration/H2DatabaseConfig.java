package be.kdg.pro3.profilecode.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Configuration
@Profile("dev")
public class H2DatabaseConfig {



	@Bean
	public DataSource dataSource(){
		DataSource dataSource = DataSourceBuilder
			.create()
		.driverClassName("org.h2.Driver")
			.url("jdbc:h2:mem:studentdb")
			.username("sa")
			.password("")
			.build();
		return dataSource;
	}

}
