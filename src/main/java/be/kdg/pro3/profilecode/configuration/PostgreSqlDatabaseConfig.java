package be.kdg.pro3.profilecode.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Configuration
@Profile("prod")
public class PostgreSqlDatabaseConfig {


	@Bean
	public DataSource dataSource(){
		DataSource dataSource = DataSourceBuilder
			.create()
			.driverClassName("org.postgresql.Driver")
			.url("jdbc:postgresql:pro3_db")
			.username("postgres")
			.password("student_1234")
			.build();
		return dataSource;
	}



}
